import React from 'react'

export default class Lamp extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  renderViewMode() {
    const { id, name, led,lumenus, deleteLamp } = this.props;
    return (
      <div>
        <span>
          {id} - {name} - {led} - {lumenus}
        </span>
        <button onClick={this.toggleEditMode} className="success">
          edit
        </button>
        <button onClick={() => deleteLamp(id)} className="warning">
          x
        </button>
      </div>
    );
  }
  renderEditMode() {
    const { name, led,lumenus,updateLamp } = this.props;
    return (
      <form
        className="third"
        onSubmit={this.onSubmit}
        onReset={this.toggleEditMode}
      >
        <input
          type="text"
          placeholder="name"
          name="Lamp_name_input"
          defaultValue={name}
        />
        <input
          type="text"
          placeholder="led"
          name="Lamp_led_input"
          defaultValue={led}
        />
          <input
          type="text"
          placeholder="lumenus"
          name="Lamp_lumenus_input"
          defaultValue={lumenus}
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  }
  onSubmit = evt => {
    // stop the page from refreshing
    evt.preventDefault();
    // target the form
    const form = evt.target;
    // extract the two inputs from the form
    const Lamp_name_input = form.Lamp_name_input;
    const Lamp_led_input = form.Lamp_led_input;
    const Lamp_lumenus_input = form.Lamp_lumenus_input;

    // extract the values
    const name = Lamp_name_input.value;
    const led = Lamp_led_input.value;
    const lumenus = Lamp_lumenus_input.value;

    // get the id and the update function from the props
    const { id, updateLamp } = this.props;
    // run the update Lamp function
    updateLamp(id, { name, led,lumenus });
    // toggle back view mode
    this.toggleEditMode();
  };
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
    }
  }
}
