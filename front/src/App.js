import React, { Component } from "react";
import Lamp from "./Lamp";
import "./App.css";
import { pause } from './utils.js'
import { ToastContainer, toast } from 'react-toastify';
import { Transition } from 'react-spring'
import 'react-toastify/dist/ReactToastify.css';
import { withRouter, Switch, Route, Link } from "react-router-dom";
import LampList from "./LampList";





class App extends Component {
  state = {
    lamps_list: [],
    error_message: "",
    name: "",
    led: "",
    lumenus:"",
    isLoading:false
  };


  getLamp = async id => {
    // check if we already have the lamp
    const previous_lamp = this.state.lamps_list.find(
      lamp => lamp.id === id
    );
    if (previous_lamp) {
      return; // do nothing, no need to reload a lamp we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/lamps/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of lamps
        const lamp = answer.result;
        const lamps_list = [...this.state.lamps_list, lamp];
        this.setState({ lamps_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteLamp = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/lamps/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const lamps_list = this.state.lamps_list.filter(
          lamp => lamp.id !== id
        );
        this.setState({ lamps_list });
        toast("lamp deleted")
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateLamp = async (id, props) => {
    try {

      const response = await fetch(
        `http://localhost:8080/lamps/update/${id}?name=${props.name}&led=${props.led}&lumenus=${props.lumenus}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const lamps_list = this.state.lamps_list.map(lamp => {
          // if this is the lamp we need to change, update it. This will apply to exactly
          // one lamp2
          if (lamp.id === id) {
            const new_lamp = {
              id: lamp.id,
              name: props.name || lamp.name,
              led: props.led|| lamp.led,
              lumenus: props.lumenus || lamp.lumenus
            };
            toast("lamp updated")
            return new_lamp;
            
          }
          // otherwise, don't change the lamp at all
          else {
            return lamp;

          }
        });
        this.setState({ lamps_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createlamp = async props => {
    try {

      const { name, led,lumenus } = props;
      const response = await fetch(
        `http://localhost:8080/lamps/add/?name=${name}&led=${led}&lumenus=${lumenus}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const lamp = { name, led, id };
        const lamps_list = [...this.state.lamps_list, lamp];
        this.setState({ lamps_list });
        toast("lamp added")
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getlampsList = async order => {
    try {
      this.setState({ isLoading: true });
      const response = await fetch(
        `http://localhost:8080/lamps/list?order=${order}`
      );
      await pause()
      const answer = await response.json();
      if (answer) {
        const lamps_list = answer;
        
        this.setState({ lamps_list,isLoading:false });
       toast("lamp loaded")

      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  componentDidMount() {
    this.getlampsList();
  }
  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract name and led from state
    const { name, led,lumenus } = this.state;
    // create the lamp from mail and led
    this.createlamp({ name, led,lumenus });
    // empty name and led so the text input fields are reset
    this.setState({ name: "", led: "",lumenus:"" });
  };
  renderHomePage = () => {
    const { lamps_list, error_message ,isLoading} = this.state;
    return (
      <div className="App">
        {error_message ? <p> ERROR! {error_message}</p> : false}

        {isLoading ? (
          <p>loading...</p>
        ) : (
          <Transition
            items={lamps_list}
            keys={lamp => lamp.id}
            from={{ transform: "translate3d(-100px,0,0)" }}
            enter={{ transform: "translate3d(0,0px,0)" }}
            leave={{ transform: "translate3d(-100px,0,0)" }}
          >
            {lamp => style => (
              <div style={style}>
                <Lamp
                  key={lamp.id}
                  id={lamp.id}
                  name={lamp.name}
                  led={lamp.led}
                  lumenus={lamp.lumenus}
                  updateLamp={this.updateLamp}
                  deleteLamp={this.deleteLamp}
                  />
              </div>
            )}
          </Transition>
        )}


        <ToastContainer />

      </div>
    );
  }

  renderProfilePage = () => {
    return <div>profile page</div>;
  };

  renderCreateForm = () =>(
    <form className="third" onSubmit={this.onSubmit}>
    <input
      type="text"
      placeholder="name"
      onChange={evt => this.setState({ name: evt.target.value })}
      value={this.state.name}
    />
    <input
      type="text"
      placeholder="led"
      onChange={evt => this.setState({ led: evt.target.value })}
      value={this.state.led}
    />
              <input
      type="text"
      placeholder="lumenus"
      onChange={evt => this.setState({ lumenus: evt.target.value })}
      value={this.state.lumenus}
    />
    <div>
      <input type="submit" value="ok" />
      <input type="reset" value="cancel" className="button" />
    </div>
  </form>)
  

  renderLamp() {
    if (this.state.isLoading) {
      return <p>loading...</p>;
    }

  return (
    <Switch>
      <Route path="/" exact render={this.renderHomePage} />
      <Route path="/contact/:id" render={this.renderLampPage} />
      <Route path="/profile" render={this.renderProfilePage} />
      <Route path="/create" render={this.renderCreateForm} />
      <Route render={() => <div>not found!</div>} />
    </Switch>
  );
}

  render(){
    return(
    <div className="App">
    <div>
      <Link to="/">Home</Link> |<Link to="/profile">profile</Link> |
      <Link to="/create">create</Link>
    </div>
    {this.renderLamp()}
    <ToastContainer />
  </div>)

  }
}

export default withRouter(App)

