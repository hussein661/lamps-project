// front/src/LampList.js
import React from "react";
import { Transition } from "react-spring"; // you can remove this from App.js
import { Link } from "react-router-dom";

const LampList = ({ lamps_list }) => (
  <Transition
    items={lamps_list}
    keys={lamp => lamp.id}
    from={{ transform: "translate3d(-100px,0,0)" }}
    enter={{ transform: "translate3d(0,0px,0)" }}
    leave={{ transform: "translate3d(-100px,0,0)" }}
  >
    { lamp => style => (
      <div style={style}>
        <Link to={"/lamp/"+lamp.id}>{lamp.name}</Link>
      </div>
    )}
  </Transition>
);
