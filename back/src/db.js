// back/src/db.js
import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

// back/src/db.js
const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  

  const addLamp = async (props) => {

    const { name, led,lumenus } = props
    try{
      const result = await db.run(SQL`INSERT INTO lamps (name,led,lumenus) VALUES (${name}, ${led},${lumenus})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  
  const deleteLamp = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM lamps WHERE lamp_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`lamp "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the lamp "${id}": `+e.message)
    }
  }
  
  const updateLamp = async (id, props) => {

    const { name, led, lumenus } = props;
    try {
      let statement = "";
        statement = SQL`UPDATE lamps SET  name=${name} , led=${led} , lumenus = ${lumenus} WHERE lamp_id = ${id}`;
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the lamp ${id}: ` + e.message);
    }
  }


  const getLamp = async (id) => {
    try{
      const lampList = await db.all(SQL`SELECT lamp_id AS id, name,led,lumenus FROM lamps WHERE lamp_id = ${id}`);
      const lamp = lampList[0]
      return lamp
    }catch(e){
      throw new Error(`couldn't get the lamp ${id}: `+e.message)
    }
  }
  
  const getAllLamps = async (orderBy) => {
    try{
      
      let statement = `SELECT lamp_id AS id, name, led,lumenus FROM lamps`
      switch(orderBy){
        case 'name': statement+= ` ORDER BY name`; break;
        case 'lumenus': statement+= ` ORDER BY lumenus`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve lamps: `+e.message)
    }
  }
  
  const controller = {
    addLamp,
    deleteLamp,
    updateLamp,
    getLamp,
    getAllLamps
  }

  return controller
}


export default initializeDatabase


