// back/src/index.js
import app from './app'
import initializeDatabase from './db'
import { EDEADLK } from 'constants';

const start = async () => {
  const controller = await initializeDatabase()
  
  app.get('/', (req, res, next) => res.send("ok"));

  // CREATE
  app.get('/lamps/add', async (req, res, next) => {
    const { name, led,lumenus} = req.query
    const result = await controller.addLamp({name,led,lumenus})
    res.json({success:true, result})
  })

  // READ
  app.get('/lamps/get/:id', async (req, res, next) => {
    const { id } = req.params
    const lamp = await controller.getLamp(id)
    res.json({success:true, result:lamp})
  })

  // DELETE
  app.get('/lamps/delete/:id', async (req, res, next) => {
    const { id } = req.params
    const result = await controller.deleteLamp(id)
    res.json({success:true, result})
  })

  // UPDATE
  app.get('/lamps/update/:id', async (req, res, next) => {
    const { id } = req.params
    const { name, led,lumenus } = req.query
    const result = await controller.updateLamp(id,{name,led,lumenus})
    res.json({success:true, result})
  })

  // LIST
  app.get('/lamps/list', async (req, res, next) => {
    const { order } = req.query
    const lamps = await controller.getAllLamps(order)
    res.json(lamps)
  })

  app.listen(8080, () => console.log('server listening on port 8080'))
}

start()

